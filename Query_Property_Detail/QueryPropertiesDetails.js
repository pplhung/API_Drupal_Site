let jsonData = pm.response.json();
console.log(jsonData.data);
let queryCurrency = "USD";
let queryMeasureUnit = "Square Foot";
let propertySFID = "a0gO000001pjTjhIAE";

if (JSON.stringify(jsonData.data) == undefined) {
  console.log("The response is undefined");
} else {
  console.log("The response is not undefined");
}

pm.test("Successful POST request", function () {
  pm.expect(pm.response.code).to.be.oneOf([200, 201, 202]);
});

pm.test("response should be okay to process", function () {
  pm.response.to.not.be.error;
  pm.response.to.be.withBody;
  pm.response.to.not.have.jsonBody("error");
});

pm.test("CheckCurrency", function () {
  pm.expect(jsonData.data.queryCurrency).to.eql(queryCurrency);
  console.log(jsonData.data.queryCurrency);
});

pm.test("CheckMeasureUnit", function () {
  pm.expect(jsonData.data.queryMeasureUnit).to.eql(queryMeasureUnit);
  console.log(jsonData.data.queryMeasureUnit);
});

pm.test("CheckpropertySFI", function () {
  pm.expect(jsonData.data.propertySFID).to.eql(propertySFID);
  console.log(jsonData.data.propertySFID);
});

pm.test("Response Body contains specific strings", function () {
  pm.expect(pm.response.text()).to.include(
    "id",
    "queryCurrency",
    "queryMeasureUnit",
    "propertySFID",
    "propertyName",
    "location",
    "firstLevelLocation",
    "secondLevelLocation",
    "thirdLevelLocation"
  );
});
